//
//  PCPhotoCell.m
//  PhotoCollage
//
//  Created by framez on 15.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import "PCPhotoCell.h"
#import "UIImageView+AFNetworking.h"

@interface PCPhotoCell()

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property (strong, nonatomic) UIImage *selectedStateImage;
@property (strong, nonatomic) UIImage *unselectedStateImage;
@property (strong, nonatomic) NSURL *photoUrl;
@property (nonatomic) BOOL checked;

- (IBAction)selectButtonTap:(id)sender;

@end

@implementation PCPhotoCell

#pragma mark - Lifecycle

- (void)awakeFromNib {
    self.selectedStateImage = [UIImage imageNamed:@"select-icon"];
    self.unselectedStateImage = [UIImage imageNamed:@"select-gray-icon"];
}

#pragma mark - IBActions

- (IBAction)selectButtonTap:(id)sender {
    self.checked = !self.checked;
    [self setSelectedButtonChecked:self.checked];
    if ([self.delegate respondsToSelector:@selector(selectedPhoto:)]) {
        [self.delegate selectedPhoto:self.photoUrl];
    }
}

#pragma mark - Public

- (void)setPhotoWithUrl:(NSURL *)photoUrl selected:(BOOL)selected {
    self.photoUrl = photoUrl;
    self.checked = selected;
    [self setSelectedButtonChecked:self.checked];
    [self.photoImageView setImageWithURL:self.photoUrl];
}

#pragma mark - Private

- (void)setSelectedButtonChecked:(BOOL)checked {
    UIImage *image = checked ? self.selectedStateImage : self.unselectedStateImage;
    [self.selectButton setImage:image forState:UIControlStateNormal];
}

#pragma mark - UICollectionReusableView

- (void)prepareForReuse {
    [super prepareForReuse];

    self.photoImageView.image = nil;
    [self setSelectedButtonChecked:NO];
}

@end
