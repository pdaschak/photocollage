//
//  PCPhotoCell.h
//  PhotoCollage
//
//  Created by framez on 15.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PCPhotoCellDelegate <NSObject>

- (void)selectedPhoto:(NSURL *)photoUrl;

@end

@interface PCPhotoCell : UICollectionViewCell

@property (weak, nonatomic) id<PCPhotoCellDelegate> delegate;

- (void)setPhotoWithUrl:(NSURL *)photoUrl selected:(BOOL)selected;

@end
