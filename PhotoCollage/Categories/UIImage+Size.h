//
//  UIImage+Size.h
//  PhotoCollage
//
//  Created by framez on 15.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Size)

- (UIImage *)imageScaledToSize:(CGSize)newSize;

- (UIImage *)scaleImageToSize:(CGSize)newSize;
@end
