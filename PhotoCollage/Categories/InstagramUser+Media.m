//
//  InstagramUser+Media.m
//  PhotoCollage
//
//  Created by framez on 14.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import "InstagramUser+Media.h"
#import "AppDelegate.h"
#import <InstagramKit/InstagramEngine.h>
#import "InstagramPaginationInfo.h"


@interface InstagramUser()

@property (strong, nonatomic) NSArray *recentMedia;
@property (nonatomic, copy) InstagramMediaBlock successBlock;

@end

@implementation InstagramUser (Media)

#pragma mark - Custom Accessors

- (void)setSuccessBlock:(InstagramMediaBlock)successBlock {
    _successBlock = successBlock;
}

- (InstagramMediaBlock)successBlock {
    return _successBlock;
}

#pragma mark - Public

- (void)loadAllMediaWithSuccess:(void (^)())success failure:(void (^)())failure {
    __weak typeof(self) weakSelf = self;

    self.successBlock = ^void(NSArray *media, InstagramPaginationInfo *paginationInfo) {
                weakSelf.recentMedia = [weakSelf.recentMedia arrayByAddingObjectsFromArray:media];
                if (paginationInfo) {
                    [weakSelf loadAllItemsWithPagination:paginationInfo success:weakSelf.successBlock failure:^(NSError *error) {
                        if (failure) {
                            failure();
                        }
                    }];
                } else if (success) {
                    success();
                }
            };

    [[InstagramEngine sharedEngine] getMediaForUser:self.Id withSuccess:^(NSArray *media, InstagramPaginationInfo *paginationInfo) {
        weakSelf.recentMedia = media;

        [weakSelf loadAllItemsWithPagination:paginationInfo success:weakSelf.successBlock failure:^(NSError *error) {
            if (failure) {
                failure();
            }
        }];
    } failure:^(NSError *error) {
        failure();
    }];
}

- (NSArray *)mostLikedMediaCount:(NSUInteger)mediaCount isVideo:(BOOL)isVideo {
    if (!self.recentMedia) {
        return nil;
    }
    if ([self.recentMedia count] < mediaCount) {
        mediaCount = [self.recentMedia count];
    }
    NSPredicate *imagePredicate = [NSPredicate predicateWithFormat:@"isVideo == %d", isVideo];
    NSArray *images = [self.recentMedia filteredArrayUsingPredicate:imagePredicate];

    NSSortDescriptor *mostLikedSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"likesCount" ascending:NO];
    images = [images sortedArrayUsingDescriptors:@[mostLikedSortDescriptor]];

    if (mediaCount) {
        images = [images subarrayWithRange:NSMakeRange(0, mediaCount)];
    }

    return images;
}

#pragma mark - Private

- (void)loadAllItemsWithPagination:(InstagramPaginationInfo *)paginationInfo success:(InstagramMediaBlock)success failure:(InstagramFailureBlock)failure {
    [[InstagramEngine sharedEngine] getPaginatedItemsForInfo:paginationInfo withSuccess:(InstagramMediaBlock) success failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

@end
