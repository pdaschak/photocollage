//
//  InstagramUser+Media.h
//  PhotoCollage
//
//  Created by framez on 15.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import "InstagramUser.h"

@interface InstagramUser (Media)

- (void)loadAllMediaWithSuccess:(void (^)())success failure:(void (^)())failure;
- (NSArray *)mostLikedMediaCount:(NSUInteger)mediaCount isVideo:(BOOL)isVideo;

@end
