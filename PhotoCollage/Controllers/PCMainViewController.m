//
//  ViewController.m
//  PhotoCollage
//
//  Created by framez on 12.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import "PCMainViewController.h"
#import "InstagramKit.h"
#import "InstagramUser+Media.h"
#import "PCPickerViewController.h"
#import "SVProgressHUD.h"

@interface PCMainViewController ()

@property (weak, nonatomic) IBOutlet UIButton *collageButton;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;

@property (strong, nonatomic) InstagramUser *user;

- (IBAction)collageTap:(id)sender;

@end

@implementation PCMainViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.usernameTextField addTarget:self action:@selector(usernameChanged) forControlEvents:UIControlEventEditingChanged];
    [self usernameChanged];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.usernameTextField resignFirstResponder];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - IBActions

- (IBAction)collageTap:(id)sender {
    __weak typeof(self) weakSelf = self;

    [SVProgressHUD show];
    [[InstagramEngine sharedEngine] searchUsersWithString:[self username] withSuccess:^(NSArray *users, InstagramPaginationInfo *paginationInfo) {
        for (InstagramUser *user in users) {
            if ([user.username isEqualToString:[weakSelf username]]) {
                weakSelf.user = user;
                [weakSelf.user loadAllMediaWithSuccess:^{
                    [SVProgressHUD dismiss];
                    [weakSelf performSegueWithIdentifier:@"pickerSegue" sender:self];
                }                              failure:^{
                    [SVProgressHUD dismiss];
                    [weakSelf showAlertWithTitle:@"Ошибка" message:@"Произошла ошибка при загрузке данных"];
                }];
            }
        }
    }                                                                         failure:^(NSError *error) {
        [SVProgressHUD dismiss];
        [weakSelf showAlertWithTitle:@"Ошибка" message:@"Произошла ошибка сети или пользователь не найден."];
    }];
}

#pragma mark - Private

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
}

- (void)usernameChanged {
    self.collageButton.enabled = (BOOL) [[self username] length];
}

- (NSString *)username {
    return [self.usernameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

#pragma mark - Keyboard notification

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];

    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGFloat viewHeight = CGRectGetHeight(self.view.frame);
    CGFloat collageButtontMaxY = CGRectGetMaxY(self.collageButton.frame);

    if (collageButtontMaxY >  viewHeight - keyboardSize.height) {
        [UIView animateWithDuration:0.4 animations:^{
            self.view.center = CGPointMake(self.view.center.x, self.view.center.y - (collageButtontMaxY - (viewHeight - keyboardSize.height)));
        }];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];

    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGFloat viewHeight = CGRectGetHeight(self.view.frame);
    CGFloat collageButtontMaxY = CGRectGetMaxY(self.collageButton.frame);

    if (collageButtontMaxY >  viewHeight - keyboardSize.height) {
        [UIView animateWithDuration:0.4 animations:^{
            self.view.center = CGPointMake(self.view.center.x, CGRectGetHeight(self.view.frame) / 2);
        }];
    }
}

#pragma mark - UIViewController

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    if ([segue.identifier isEqualToString:@"pickerSegue"]) {
        PCPickerViewController *pickerViewController = segue.destinationViewController;
        pickerViewController.photos = [self.user mostLikedMediaCount:10 isVideo:NO];
    }
}

@end
