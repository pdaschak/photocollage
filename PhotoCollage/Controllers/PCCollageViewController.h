//
//  PCCollageViewController.h
//  PhotoCollage
//
//  Created by framez on 15.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCCollageViewController : UIViewController

@property (strong, nonatomic) NSArray *collagePhotos;

@end
