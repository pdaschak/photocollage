//
//  PCPickerViewController.m
//  PhotoCollage
//
//  Created by framez on 15.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import "PCPickerViewController.h"
#import "CoreGraphics/CoreGraphics.h"
#import "InstagramMedia.h"
#import "PCCollageViewController.h"

@interface PCPickerViewController ()

@property (strong, nonatomic) NSMutableArray *selectedPhotos;

@end

@implementation PCPickerViewController

static NSString * const reuseIdentifier = @"PhotoCell";

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    self.selectedPhotos = [NSMutableArray new];

    [self.collectionView registerNib:[UINib nibWithNibName:@"PCPhotoCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
    if ([self.collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]]) {
        UICollectionViewFlowLayout *collectionViewFlowLayout = (UICollectionViewFlowLayout *) self.collectionViewLayout;
        collectionViewFlowLayout.itemSize = CGSizeMake(CGRectGetWidth(self.collectionView.bounds) / 2, CGRectGetWidth(self.collectionView.bounds) / 2);
    }

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Коллаж" style:UIBarButtonItemStylePlain target:self action:@selector(collageButtonTap)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - Private

- (void)collageButtonTap {
    if (![self.selectedPhotos count]) {
        [self showAlertWithTitle:@"Информация" message:@"Необходимо выбрать не менее одной фотографии "];
    } else if ([self.selectedPhotos count] > 7) {
        [self showAlertWithTitle:@"Информация" message:@"Можно выбрать не более 7 фотографий"];
    } else {
        [self performSegueWithIdentifier:@"collageSegue" sender:self];
    }
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - PCPhotoCell delegate

- (void)selectedPhoto:(NSURL *)photoUrl {
    NSPredicate *thumbnailPhotoPredicate = [NSPredicate predicateWithFormat:@"thumbnailURL == %@", photoUrl];
    InstagramMedia *instagramMedia = [self.photos filteredArrayUsingPredicate:thumbnailPhotoPredicate].firstObject;
    if ([self.selectedPhotos containsObject:instagramMedia]) {
        [self.selectedPhotos removeObject:instagramMedia];
    } else {
        [self.selectedPhotos addObject:instagramMedia];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"collageSegue"]) {
        PCCollageViewController *collageViewController = segue.destinationViewController;
        collageViewController.collagePhotos = self.selectedPhotos;
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.photos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    InstagramMedia *photoMedia = self.photos[indexPath.item];
    PCPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    [cell setPhotoWithUrl:photoMedia.thumbnailURL selected:[self.selectedPhotos containsObject:photoMedia]];

    return cell;
}

@end
