//
//  PCPickerViewController.h
//  PhotoCollage
//
//  Created by framez on 15.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCPhotoCell.h"

@interface PCPickerViewController : UICollectionViewController <PCPhotoCellDelegate>

@property (strong, nonatomic) NSArray *photos;

@end
