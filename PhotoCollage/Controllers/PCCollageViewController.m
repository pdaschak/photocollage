//
//  PCCollageViewController.m
//  PhotoCollage
//
//  Created by framez on 15.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import "PCCollageViewController.h"
#import "InstagramMedia.h"
#import "PCCollageMaker.h"
#import "MessageUI/MessageUI.h"
#import "SVProgressHUD.h"

@interface PCCollageViewController () <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *collageImageView;
@property (strong, nonatomic) NSMutableArray *photos;
@property (strong, nonatomic) PCCollageMaker *collageMaker;

- (IBAction)sendMailButtonTap:(id)sender;
@end

@implementation PCCollageViewController

#pragma mark - Lifecycle

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        self.photos = [NSMutableArray new];
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.collageMaker = [PCCollageMaker makerWithSize:self.collageImageView.bounds.size];
    self.collageMaker.opaque = NO;
    self.collageMaker.backgroundColor = [UIColor lightGrayColor];

    __weak typeof(self) weakSelf = self;
    [SVProgressHUD show];
    for (InstagramMedia *instagramMedia in self.collagePhotos) {
        [self loadImageWithURL:instagramMedia.standardResolutionImageURL succes:^(UIImage *image) {
            [weakSelf.photos addObject:image];
            if ([weakSelf.collagePhotos count] == [weakSelf.photos count]) {
                weakSelf.collageMaker.images = weakSelf.photos;
                weakSelf.collageImageView.image = [weakSelf.collageMaker createCollage];
                [SVProgressHUD dismiss];
            }
        }];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.hidesBarsOnTap = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.hidesBarsOnTap = YES;
}

#pragma mark - IBActions

- (IBAction)sendMailButtonTap:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposeViewController = [MFMailComposeViewController new];
        mailComposeViewController.mailComposeDelegate = self;
        [mailComposeViewController setSubject:@"Фото коллаж"];
        NSData *imageData = UIImageJPEGRepresentation(self.collageImageView.image, 1);
        [mailComposeViewController addAttachmentData:imageData mimeType:@"image/jpeg" fileName:@"photo_collage.jpg"];
        [self presentModalViewController:mailComposeViewController animated:YES];
    }
}

#pragma mark - Private

- (void)loadImageWithURL:(NSURL *)url succes:(void(^)(UIImage *image))success {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData * data = [[NSData alloc] initWithContentsOfURL: url];
        if ( data == nil )
            return;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success) {
              success([UIImage imageWithData: data]);
            }
        });
    });
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    if (result == MFMailComposeResultSent) {
        [self.navigationController dismissModalViewControllerAnimated:YES];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Сообщение не отправлено!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
    }
}

@end
