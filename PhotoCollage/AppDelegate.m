//
//  AppDelegate.m
//  PhotoCollage
//
//  Created by framez on 12.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import "AppDelegate.h"
#import "SVProgressHUD.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    return YES;
}

@end
