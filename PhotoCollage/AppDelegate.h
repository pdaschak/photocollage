//
//  AppDelegate.h
//  PhotoCollage
//
//  Created by framez on 12.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

