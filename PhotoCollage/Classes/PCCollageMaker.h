//
//  PCCollageMaker.h
//  PhotoCollage
//
//  Created by framez on 15.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import "UIKit/UIKit.h"
#import <Foundation/Foundation.h>

@interface PCCollageMaker : NSObject

@property (nonatomic, readonly) CGSize size;
@property (strong, nonatomic) NSArray *images;
@property (nonatomic) BOOL opaque;
@property (strong, nonatomic) UIColor *backgroundColor;

+ (instancetype)makerWithSize:(CGSize)size;

- (UIImage *)createCollage;

@end
