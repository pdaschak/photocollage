//
//  PCCollageMaker.m
//  PhotoCollage
//
//  Created by framez on 15.02.15.
//  Copyright (c) 2015 PD. All rights reserved.
//

#import "PCCollageMaker.h"
#import "UIImage+Size.h"

@interface PCCollageMaker()

@property (strong, nonatomic) NSMutableArray *preparedImages;
@property (strong, nonatomic) UIImage *centerPhoto;
@property (nonatomic) BOOL isNeedCenterPhoto;

@end

@implementation PCCollageMaker

#pragma mark - Lifecycle

- (instancetype)initWithSize:(CGSize)size {
    self = [super init];
    if (self) {
        _size = size;
        self.isNeedCenterPhoto = NO;
        self.opaque = YES;
        self.preparedImages = [NSMutableArray new];
    }

    return self;
}

+ (instancetype)makerWithSize:(CGSize)size {
    return [[self alloc] initWithSize:size];
}

#pragma mark - Public

- (UIImage *)createCollage {
    [self prepareimages];

    UIGraphicsBeginImageContextWithOptions(self.size, self.opaque, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, self.backgroundColor.CGColor);

    CGFloat x = 0;
    CGFloat y = 0;
    for (UIImage *image in self.preparedImages) {
        [image drawAtPoint:CGPointMake(x, y)];
        x += image.size.width;
        if (x >= self.size.width) {
            x = 0;
            y += image.size.height;
        }
    }

    if (self.centerPhoto) {
        [self.centerPhoto drawAtPoint:CGPointMake((self.size.width - self.centerPhoto.size.width) / 2, (self.size.height - self.centerPhoto.size.height) / 2)];
    }

    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return resultImage;
}

#pragma mark - Private

- (void)prepareimages {
    CGSize imageSize;
    if ([self.images count] <= 3) {
        imageSize = CGSizeMake(self.size.width, self.size.height / [self.images count]);
    } else {
        imageSize = CGSizeMake(self.size.width / 2, self.size.height / ([self.images count] / 2));
    }
    self.isNeedCenterPhoto = (([self.images count] % 2) == 1) && ([self.images count] > 3);

    NSInteger index = 0;
    if (self.isNeedCenterPhoto) {
        self.centerPhoto = [self.images[0] scaleImageToSize:CGSizeMake(self.size.width / 2.3, self.size.height / 2.3)];
        index = 1;
    }

    for (index; index < [self.images count]; index++) {
        UIImage *image = self.images[index];
        [self.preparedImages addObject:[image scaleImageToSize:imageSize]];
    }
}


@end
